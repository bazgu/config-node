exports.port = 7600
exports.host = '127.0.0.1'

exports.accountNodes = [{
    host: '127.0.0.1',
    port: 7300,
}]

exports.captchaNodes = [{
    host: '127.0.0.1',
    port: 7200,
}]

exports.fileNodes = [{
    host: '127.0.0.1',
    port: 7500,
}]

exports.sessionNodes = [{
    host: '127.0.0.1',
    port: 7400,
}]

exports.log = {
    http: true,
}
