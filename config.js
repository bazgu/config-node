module.exports = {

    debug_mode: true,
    listen: {
        port: 7600,
        host: '127.0.0.1',
    },

    accountNodes: [{
        host: '127.0.0.1',
        port: 7300,
    }],

    captchaNodes: [{
        host: '127.0.0.1',
        port: 7200,
    }],

    fileNodes: [{
        host: '127.0.0.1',
        port: 7500,
    }],

    sessionNodes: [{
        host: '127.0.0.1',
        port: 7400,
    }],

}
