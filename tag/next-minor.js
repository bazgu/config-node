#!/usr/bin/env node
process.chdir(__dirname)

var child_process = require('child_process')

var ReadText = require('../lib/ReadText.js')

var git = child_process.spawn('git', ['tag', '--contains'])
ReadText(git.stdout, text => {

    if (text !== '') {
        console.log('ERROR: Later tags exist')
        process.exit(1)
    }

    var git = child_process.spawn('git', ['describe', '--no-abbrev'])
    ReadText(git.stdout, text => {

        if (!text.match(/^\d+\.\d+\n$/)) {
            console.log('ERROR: Failed to parse current tag')
            process.exit(1)
        }

        var nextTag = text.replace(/\.(\d+)\n$/, (a, b) => {
            return '.' + (parseInt(b, 10) + 1)
        })
        var git = child_process.spawn('git', ['tag', nextTag, '-m', nextTag])

    })

})
