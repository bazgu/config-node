process.chdir(__dirname)

var version = process.argv[2]

var url = require('url')

var Error404Page = require('./lib/Error404Page.js'),
    Log = require('./lib/Log.js')

var pages = Object.create(null)
pages['/get'] = require('./lib/GetPage.js')
pages['/node'] = require('./lib/NodePage.js')(version)

require('./lib/Server.js')((req, res) => {
    Log.http(req.method + ' ' + req.url)
    var parsedUrl = url.parse(req.url, true)
    var page = pages[parsedUrl.pathname]
    if (page === undefined) page = Error404Page
    page(res)
})
