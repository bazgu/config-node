#!/bin/bash
cd `dirname $BASH_SOURCE`

if [ ! -f server.pid ]
then
    exit
fi

pid=`cat server.pid`

if ps -p $pid > /dev/null
then
    echo "INFO: Killing process $pid ..."
    kill $pid
fi

./clean.sh
