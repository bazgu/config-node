var config = require('../config.js')

module.exports = res => {
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({
        accountNodes: config.accountNodes,
        captchaNodes: config.captchaNodes,
        fileNodes: config.fileNodes,
        sessionNodes: config.sessionNodes,
    }))
}
