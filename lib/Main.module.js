const url = require('url')

module.exports = app => () => {

    const listen = app.config.listen

    const pages = Object.create(null)
    pages['/get'] = app.GetPage

    require('http').createServer((req, res) => {

        app.log.info('Http ' + req.method + ' ' + req.url)

        const parsed_url = url.parse(req.url, true)
        const request = {
            parsed_url, req, res,
            respond (response) {
                app.EchoText(request, {
                    type: 'application/json',
                    content: JSON.stringify(response),
                })
            },
        }

        const page = (() => {
            const page = pages[parsed_url.pathname]
            if (page !== undefined) return page
            return app.Error404Page
        })()

        page(request)

    }).listen(listen.port, listen.host)

    app.Watch(['lib'])

    app.log.info('Started')
    app.log.info('Listening http://' + listen.host + ':' + listen.port + '/')

}
