module.exports = app => request => {
    request.respond({
        accountNodes: app.config.accountNodes,
        captchaNodes: app.config.captchaNodes,
        fileNodes: app.config.fileNodes,
        sessionNodes: app.config.sessionNodes,
    })
}
