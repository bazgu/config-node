module.exports = version => {

    var content = JSON.stringify({
        software: 'config-node',
        version: version,
    })

    return res => {
        res.setHeader('Content-Type', 'application/json')
        res.end(content)
    }

}
