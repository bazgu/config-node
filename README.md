Bazgu Config Node
=================

This program **config-node** is one of the nodes in a Bazgu infrastructure.
A config-node contains and serves the list of all the other nodes.

Workflow
--------

Account-nodes, front-nodes, session-nodes and file-nodes ask
a config-node for the list of other nodes at startup.
After that the config-node does nothing.

Scripts
-------

* `./restart.sh` - start/restart the server.
* `./stop.sh` - stop the server.
* `./clean.sh` - clean the server after an unexpected shutdown.
* `./rotate.sh` - clean old logs.

Configuration
-------------

`config.js` contains the configuration.
