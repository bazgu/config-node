function run () {
    app.log.info('Starting')
    node = child_process.spawn('node', ['index.js'])
    node.stdout.pipe(process.stdout)
    node.stderr.pipe(process.stderr)
    node.on('exit', code => {
        app.Timeout(run, code === 0 ? 200 : 2000)
    })
}

process.chdir(__dirname)

const child_process = require('child_process')

const app = require('./lib/App')()

let node
run()

process.on('SIGTERM', () => {
    node.kill()
    process.exit()
})
